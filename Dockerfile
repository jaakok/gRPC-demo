FROM golang:latest

CMD curl -LO https://github.com/protocolbuffers/protobuf/releases/download/v3.15.5/protoc-3.15.5-linux-x86_64.zip
CMD unzip protoc-3.15.5-linux-x86_64.zip -d $HOME/.local
CMD export PATH=$PATH:/usr/.local/bin/

RUN "protoc --version"